﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace minesWeeper
{
    class modelManager
    {

        public int mines { get; set; }
        public int fieldValue { get; set; }
        public int start_x { get; set; }
        public int start_y { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int flags { get; set; }
        public int flag_value { get; set; }
        public int buttonSize { get; set; }
        public int distance_between { get; set; }
        public int radiostate { get; set; }
        public int seconds { get; set; }
        public int minutes { get; set; }
        public bool gameover { get; set; }
        public bool firstPlay { get; set; }
        public Point coord { get; set; }

        public Button[,] btn = new Button[41, 41];

        public int[,] btn_prop = new int[41, 41];

        public int[,] saved_btn_prop = new int[41, 41];

        public int[] dx8 = { 1, 0, -1, 0, 1, -1, -1, 1 };

        public int[] dy8 = { 0, 1, 0, -1, 1, -1, 1, -1 };
       
    }
}
