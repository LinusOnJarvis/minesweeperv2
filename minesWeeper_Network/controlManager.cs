﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace minesWeeper
{
    class controlManager
    {
        public void radioSelection(int radiostate)
        {
            modelManager model = new modelManager();
            if (radiostate == 0)
            {
                model.mines = 10;
                model.fieldValue = 9;
                model.buttonSize = 50;
                model.distance_between = 51;
            }
            if (radiostate == 1)
            {
                model.buttonSize = 35;
                model.distance_between = 36;
                model.mines = 40;
                model.fieldValue = 16;
            }
        }
        public void XML_writer(string playerName,int level,int timerMap)
        {

        }
        public void XML_reader(string playerName)
        {

        }
        public void mineLOcation(int randMine, Button button)
        {

        }
        public void clickCheck(int clickCoordinate, Button button)
        {

        }
        public void numberLocation(int randMine, Button button)
        {

        }
        public void gameProgress(int counter, PictureBox imgProgess,int level,int toplisteasy,int toplistMedium)
        {

        }
    }
}
