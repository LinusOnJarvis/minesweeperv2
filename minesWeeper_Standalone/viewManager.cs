﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace minesWeeper
{
    public partial class viewManager : Form
    {
        modelManager model = new modelManager();
        controlManager control = new controlManager();

        public viewManager()
        {
            InitializeComponent();

        }
        private void viewManager_Load(object sender, EventArgs e)
        {
            groupRecord.Visible = false;
            gTopListEasy.Visible = false;
            gTopListMedium.Visible = false;
            model.firstPlay = true;
            model.gameover = false;
            model.flag_value = 10;
        }
        private void buttonPlay_Click(object sender, EventArgs e)
        {
            groupRecord.Visible = true;
            gTopListEasy.Visible = true;
            gTopListMedium.Visible = true;
            timerState.Text = "Timer Enabled";
            timerState.ForeColor = Color.Green;

            if (radioEasy.Checked)
            {
                model.mines = 10;
                model.fieldValue = 9;
                model.buttonSize = 50;
                model.distance_between = 51;
                model.radiostate = 0;
               // control.radioSelection(model.radiostate);
                SetDimensions(9);
                TableMargins(9, 9);
            }
            if (radioMedium.Checked)
            {
                model.buttonSize = 35;
                model.distance_between = 36;
                model.mines = 40;
                model.fieldValue = 16;
                SetDimensions(16);
                model.radiostate = 1;
                TableMargins(16, 16);
            }

            if (model.firstPlay)
            {
                StartGame();
                model.firstPlay = false;
            }
            else
                if (!model.firstPlay)
            {
                listBoxRecord.Items.Clear();
                if (model.radiostate == 0)
                {
                    ResetGame(9, 9);
                }
                if (model.radiostate == 1)
                {
                    ResetGame(16, 16);
                }

                StartGame();
            }
        }
        void set_ButtonImage(int x, int y)
        {
            model.btn[x, y].Enabled = false;
            model.btn[x, y].BackgroundImageLayout = ImageLayout.Stretch;

            if (model.gameover && model.btn_prop[x, y] == model.flag_value)
                model.btn_prop[x, y] = model.saved_btn_prop[x, y];

            if (model.gameover)
                timer1.Stop();

            switch (model.btn_prop[x, y])
            {
                case 0:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources.blank;
                    EmptySpace(x, y);
                    break;

                case 1:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._1;
                    break;

                case 2:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._2;
                    break;

                case 3:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._3;
                    break;

                case 4:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._4;
                    break;

                case 5:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._5;
                    break;

                case 6:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._6;
                    break;

                case 7:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._7;
                    break;

                case 8:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources._8;
                    break;

                case -1:
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources.bomb;
                    if (!model.gameover)
                        GameOver();
                    break;
            }

        }

        int isPointOnMap(int x, int y)
        {
            if (x < 1 || x > model.width || y < 1 || y > model.height)
                return 0;
            return 1;
        }

        void EmptySpace(int x, int y)
        {
            if (model.btn_prop[x, y] == 0)
            {
                for (int i = 0; i < 8; i++)
                {
                    int cx = x + model.dx8[i];
                    int cy = y + model.dy8[i];

                    if (isPointOnMap(cx, cy) == 1)
                        if (model.btn[cx, cy].Enabled == true && model.btn_prop[cx, cy] != -1 && !model.gameover)
                        {
                            ProgressValue.Value++;
                            labelScore.Text = ProgressValue.Value.ToString();
                            set_ButtonImage(cx, cy);
                        }
                }
            }
        }

        void Discover_Map()
        {
            for (int i = 1; i <= model.width; i++)
                for (int j = 1; j <= model.height; j++)
                    if (model.btn[i, j].Enabled == true)
                    {
                        set_ButtonImage(i, j);
                    }
        }

        void GameOver()
        {
            timerState.Text = "Timer Disabled";
            timerState.ForeColor = Color.Red;
            model.gameover = true;
            Discover_Map();
            MessageBox.Show("Game Over !");
        }

        void Check_FlagWin()
        {
            bool win = true;

            for (int i = 1; i <= model.width; i++)
                for (int j = 1; j <= model.height; j++)
                    if (model.btn_prop[i, j] == -1)
                        win = false;

            if (win)
            {
                WinGame();
            }
        }

        void WinGame()
        {
            model.gameover = true;
            Discover_Map();
            ProgressValue.Value = 0;
            MessageBox.Show("Win !");
            viewUpdater update = new viewUpdater();
            update.Show();
        }

        void Check_ClickWin()
        {
            bool win = true;
            for (int i = 1; i <= model.width; i++)
                for (int j = 1; j <= model.height; j++)
                    if (model.btn[i, j].Enabled == true && model.saved_btn_prop[i, j] != -1)
                        win = false;

            if (win)
            {
                WinGame();
            }
        }

        private void OneClick(object sender, EventArgs e)
        {
            model.coord = ((Button)sender).Location;
            int x = (model.coord.X - model.start_x) / model.buttonSize;
            int y = (model.coord.Y - model.start_y) / model.buttonSize;
            listBoxRecord.Items.Add("X : " + x + " Y : " + y);

            if (model.btn_prop[x, y] != model.flag_value)
            {

                ((Button)sender).Enabled = false;
                ((Button)sender).Text = "";

                ((Button)sender).BackgroundImageLayout = ImageLayout.Stretch;

                if (model.btn_prop[x, y] != -1 && !model.gameover)
                {
                    ProgressValue.Value++;
                    labelScore.Text =  ProgressValue.Value.ToString();
                    Check_ClickWin();
                }

                set_ButtonImage(x, y);
            }
        }

        int MinesAround(int x, int y)
        {
            int score = 0;
            for (int i = 0; i < 8; i++)
            {
                int cx = x + model.dx8[i];
                int cy = y + model.dy8[i];

                if (isPointOnMap(cx, cy) == 1 && model.btn_prop[cx, cy] == -1)
                    score++;
            }
            return score;
        }

        void SetMapNumbers(int x, int y)
        {
            for (int i = 1; i <= x; i++)
                for (int j = 1; j <= y; j++)
                    if (model.btn_prop[i, j] != -1)
                    {
                        model.btn_prop[i, j] = MinesAround(i, j);
                        //Debugging
                        //btn[i, j].Text = btn_prop[i, j].ToString();
                        model.saved_btn_prop[i, j] = MinesAround(i, j);
                    }
        }

        private void RightClick(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Right)
            {

                model.coord = ((Button)sender).Location;
                int x = (model.coord.X - model.start_x) / model.buttonSize;
                int y = (model.coord.Y - model.start_y) / model.buttonSize;

                if (model.btn_prop[x, y] != model.flag_value && model.flags > 0)
                {
                    model.btn[x, y].BackgroundImageLayout = ImageLayout.Stretch;
                    model.btn[x, y].BackgroundImage = minesWeeper.Properties.Resources.flag;
                    model.btn_prop[x, y] = model.flag_value;
                    model.flags--;
                    Check_FlagWin();
                }
                else
                    if (model.btn_prop[x, y] == model.flag_value)
                {
                    model.btn_prop[x, y] = model.saved_btn_prop[x, y];
                    model.btn[x, y].BackgroundImageLayout = ImageLayout.Stretch;
                    model.btn[x, y].BackgroundImage = null;
                    model.flags++;
                }

                flagsResult.Text = model.flags.ToString();
            }
        }

        void CreateButtons(int x, int y)
        {
            for (int i = 1; i <= x; i++)
                for (int j = 1; j <= y; j++)
                {
                    model.btn[i, j] = new Button();
                    model.btn[i, j].SetBounds(i * model.buttonSize + model.start_x, j * model.buttonSize + model.start_y, model.distance_between, model.distance_between);
                    model.btn[i, j].Click += new EventHandler(OneClick);
                    model.btn[i, j].MouseUp += new MouseEventHandler(RightClick);
                    model.btn_prop[i, j] = 0;
                    model.saved_btn_prop[i, j] = 0;
                    model.btn[i, j].TabStop = false;
                    Controls.Add(model.btn[i, j]);
                }
        }

        void GenerateMap(int x, int y, int mines)
        {
            Random rand = new Random();
            List<int> coordx = new List<int>();
            List<int> coordy = new List<int>();

            while (mines > 0)
            {
                coordx.Clear();
                coordy.Clear();

                for (int i = 1; i <= x; i++)
                    for (int j = 1; j <= y; j++)
                        if (model.btn_prop[i, j] != -1)
                        {
                            coordx.Add(i);
                            coordy.Add(j);
                        }

                int randNum = rand.Next(0, coordx.Count);
                model.btn_prop[coordx[randNum], coordy[randNum]] = -1;
                model.saved_btn_prop[coordx[randNum], coordy[randNum]] = -1;
                mines--;
            }
        }

        void StartGame()
        {

            model.flags = model.mines;
            model.gameover = false;

            ProgressValue.Value = 0;
            ProgressValue.Maximum = model.height * model.width - model.mines;

            timer1.Start();
            model.seconds = 0;
            model.minutes = 0;

            flagsResult.Text = model.flags.ToString();
            labelScore.Text = 0.ToString();

            if (model.firstPlay)
                CreateButtons(model.width, model.height);

            GenerateMap(model.width, model.height, model.mines);
            SetMapNumbers(model.width, model.height);

        }

        void ResetGame(int x, int y)
        {
            for (int i = 1; i <= x; i++)
                for (int j = 1; j <= y; j++)
                {
                    model.btn[i, j].Enabled = true;
                    model.btn[i, j].Text = "";
                    model.btn[i, j].BackgroundImage = null;
                    model.btn_prop[i, j] = 0;
                    model.saved_btn_prop[i, j] = 0;
                }
        }

        void Warnings(int id)
        {
            switch (id)
            {
                case 1:
                    MessageBox.Show("Empty Fields !");
                    break;
                case 2:
                    MessageBox.Show("Wrong Input !");
                    break;

            }
        }
        void SetDimensions(int value)
        {
            model.height = value;
            model.width = value;

            if (model.height > 25)
                model.height = 25;
            else
                if (model.height < 5)
                model.height = 5;

            if (model.width > 40)
                model.width = 40;
            else
                if (model.width < 5)
                model.width = 5;


        }
        bool hasLetters(string s)
        {
            for (int i = 0; i < s.Length; i++)
                if (!Char.IsDigit(s, i))
                    return true;

            return false;
        }

        void TableMargins(int x, int y)
        {
            model.start_x = (this.Size.Width  - (model.width  + 2) * model.distance_between) / 2;
            model.start_y = (this.Size.Height - (model.height + 2) * model.distance_between) / 2;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            model.seconds++;
            if (model.seconds == 60)
            {
                model.minutes++;
                model.seconds = 0;
            }
            timerResult.Text = model.minutes + " : " + model.seconds;
        }
    }
}
