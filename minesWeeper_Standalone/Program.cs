﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace minesWeeper
{
    static class Program
    {
        public static viewManager V_Manager { get; private set; }
        public static modelManager M_Manager { get; private set; }
        public static controlManager C_Manager { get; private set; }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            V_Manager = new viewManager();
            M_Manager = new modelManager();
            C_Manager = new controlManager();

            Application.Run(V_Manager);
        }
    }
}
